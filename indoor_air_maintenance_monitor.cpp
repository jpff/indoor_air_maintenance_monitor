#include "UserInterface.hpp"
#include "SettingsData.hpp"
#include "MonitorSensors.hpp"

#define FIRMWARE_VERSION  0.1   // Manually increment with every firmware update

#define DEVMODE_LCDTEST                 0
#define DEVMODE_SETTINGS_CHECKSUMTEST   1

// TODO:
// add LDR sensor, em percentagem, adicionar campo nos settings para threshold
// add rotary encoder to set up clock
// add clock
// add air quality sensor
// RTOS
// add menus
// Saber como obter conteudos de struct com um ciclo for
// Criar class DataUnion dentro da classe SettingsData com typedef union declaro dentro e funcoes para obter a variavel T em forma de array ou na sua forma original

void* p_interface;
void* p_monitorSensors;
void* p_settingsData;

void setup()
{
  p_interface = new UserInterface;
  
  p_settingsData = new SettingsData(FIRMWARE_VERSION);
  static_cast<SettingsData*>(p_settingsData)->loadSettings();
  
  p_monitorSensors = new MonitorSensors;
}

void loop()
{
  #ifdef  DEVMODE_LCDTEST
  static_cast<UserInterface*>(p_interface)->displayShieldTest();
  #elif DEVMODE_SETTINGS_CHECKSUMTEST

  #else

  #endif
}