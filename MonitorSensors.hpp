#ifndef _MONITORSENSORS_HPP_
#define _MONITORSENSORS_HPP_

#define DHT_INDOORS_DATAPIN   39
#define DHT_OUTDOORS_DATAPIN  41
#define AIRQUALITY_DATAPIN    A14

typedef class MonitorSensors
{
  public:
    MonitorSensors();
    void enableIndoorsDHT(bool);
    void enableOutdoorsDHT(bool);
    void enableIndoorsAirQuality(bool);
    float DHT_getIndoorsTemperature();
    float DHT_getOutdoorsTemperature();
    float DHT_getIndoorsHumidity();
    float DHT_getOutdoorsHumidity();
    float DHT_getIndoorsHeatIndex(float, float);
    float DHT_getOutdoorsHeatIndex(float, float);
    float AirQuality_getIndoorsAirQuality();
  private:
    void* p_IndoorsDHTSensor;
    void* p_OutdoorsDHTSensor;
    void* p_IndoorsAirQualitySensor;
};

#endif
