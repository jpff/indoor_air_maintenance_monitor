#include "LCD1604Shield.hpp"
#include <LiquidCrystal.h>

#ifndef Arduino_h
#include <Arduino.h>
#endif

LCD1604Shield::LCD1604Shield()
{
  p_LCD1604Shield = new LiquidCrystal(LCD1604Shield::LCDPins::rs, LCD1604Shield::LCDPins::enable, LCD1604Shield::LCDPins::d4, LCD1604Shield::LCDPins::d5, LCD1604Shield::LCDPins::d6, LCD1604Shield::LCDPins::d7);
  static_cast<LiquidCrystal*>(p_LCD1604Shield)->begin(LCD1604Shield::LCDResolution::x, LCD1604Shield::LCDResolution::y);
}

bool LCD1604Shield::setLCDText(const char* LCDText, int at_x, int at_y)
{
  if (at_x >= LCD1604Shield::LCDResolution::x || at_y >= LCD1604Shield::LCDResolution::y) // Text position cannot be higher than screen dimensions
  {
    return false;
  }
  
  static_cast<LiquidCrystal*>(p_LCD1604Shield)->setCursor(at_x, at_y);
  static_cast<LiquidCrystal*>(p_LCD1604Shield)->print(LCDText);
  return true;
}

void LCD1604Shield::clearScreen()   // Stub for LiquidCrystal::clear()
{
  static_cast<LiquidCrystal*>(p_LCD1604Shield)->clear();
}

void LCD1604Shield::RunTest1604Shield()
{
  setLCDText("Press on keypad.", 0, 0);

  while(1)
  {
    static_cast<LiquidCrystal*>(p_LCD1604Shield)->setCursor(0, 1);
    
    switch (getUserInput())
    {
      case LCDButtons::RightButton:
      {
        setLCDText("Right key.      ", 0, 1);
        break;
      }
      case LCDButtons::LeftButton:
      {
        setLCDText("Left key.       ", 0, 1);
        break;
      }
      case LCDButtons::UpButton:
      {
        setLCDText("Up key.         ", 0, 1);
        break;
      }
      case LCDButtons::DownButton:
      {
        setLCDText("Down key.       ", 0, 1);
        break;
      }
      case LCDButtons::SelectButton:
      {
        setLCDText("Select key.     ", 0, 1);
        break;
      }
      case LCDButtons::NoButtonPressed:
      {
        setLCDText("No key pressed. ", 0, 1);
        break;
      }
    }
  }
}

int LCD1604Shield::readLCD_keypad()
{
  int keypad_ADC_value = analogRead(0);
  
  if (keypad_ADC_value > 1000) return LCDButtons::NoButtonPressed;
  else if (keypad_ADC_value < 50)   return LCDButtons::RightButton;
  else if (keypad_ADC_value < 195)  return LCDButtons::UpButton;
  else if (keypad_ADC_value < 380)  return LCDButtons::DownButton;
  else if (keypad_ADC_value < 555)  return LCDButtons::LeftButton;
  else if (keypad_ADC_value < 790)  return LCDButtons::SelectButton;
  else return LCDButtons::NoButtonPressed;
}

int LCD1604Shield::getUserInput()
{
  static uint16_t RightButton_debounce_16bits = 0x0000, UpButton_debounce_16bits = 0x0000, DownButton_debounce_16bits = 0x0000, LeftButton_debounce_16bits = 0x0000, SelectButton_debounce_16bits = 0x0000; // 16 bit debounce shift registers
  static uint8_t RightButton_debounce_8bits = 0x00, UpButton_debounce_8bits = 0x00, DownButton_debounce_8bits = 0x00, LeftButton_debounce_8bits = 0x00, SelectButton_debounce_8bits = 0x00; // 8 bit debounce shift registers
  
  int userInput = readLCD_keypad();

  if (LCD1604Shield::enableDebounce)
  {
    if(enable16bitDebounce)
    {
      RightButton_debounce_16bits <<= 1;
      UpButton_debounce_16bits <<= 1;
      DownButton_debounce_16bits <<= 1;
      LeftButton_debounce_16bits <<= 1;
      SelectButton_debounce_16bits <<= 1;
      
      (userInput == LCDButtons::RightButton) ? (RightButton_debounce_16bits |= 0x0001) : (RightButton_debounce_16bits &= 0xFFFE);
      (userInput == LCDButtons::UpButton) ? (UpButton_debounce_16bits |= 0x0001) : (UpButton_debounce_16bits &= 0xFFFE);
      (userInput == LCDButtons::DownButton) ? (DownButton_debounce_16bits |= 0x0001) : (DownButton_debounce_16bits &= 0xFFFE);
      (userInput == LCDButtons::LeftButton) ? (LeftButton_debounce_16bits |= 0x0001) : (LeftButton_debounce_16bits &= 0xFFFE);
      (userInput == LCDButtons::SelectButton) ? (SelectButton_debounce_16bits |= 0x0001) : (SelectButton_debounce_16bits &= 0xFFFE);
    
      if (RightButton_debounce_16bits == 0xFFFF) return LCDButtons::RightButton;
      else if (UpButton_debounce_16bits == 0xFFFF) return LCDButtons::UpButton;
      else if (DownButton_debounce_16bits == 0xFFFF) return LCDButtons::DownButton;
      else if (LeftButton_debounce_16bits == 0xFFFF) return LCDButtons::LeftButton;
      else if (SelectButton_debounce_16bits == 0xFFFF) return LCDButtons::SelectButton;
      else return LCDButtons::NoButtonPressed;
    } else {
      RightButton_debounce_8bits <<= 1;
      UpButton_debounce_8bits <<= 1;
      DownButton_debounce_8bits <<= 1;
      LeftButton_debounce_8bits <<= 1;
      SelectButton_debounce_8bits <<= 1;
    
      (userInput == LCDButtons::RightButton) ? (RightButton_debounce_8bits |= 0x01) : (RightButton_debounce_8bits &= 0xFE);
      (userInput == LCDButtons::UpButton) ? (UpButton_debounce_8bits |= 0x01) : (UpButton_debounce_8bits &= 0xFE);
      (userInput == LCDButtons::DownButton) ? (DownButton_debounce_8bits |= 0x01) : (DownButton_debounce_8bits &= 0xFE);
      (userInput == LCDButtons::LeftButton) ? (LeftButton_debounce_8bits |= 0x01) : (LeftButton_debounce_8bits &= 0xFE);
      (userInput == LCDButtons::SelectButton) ? (SelectButton_debounce_8bits |= 0x01) : (SelectButton_debounce_8bits &= 0xFE);
      
      if (RightButton_debounce_8bits == 0xFF) return LCDButtons::RightButton;
      else if (UpButton_debounce_8bits == 0xFF) return LCDButtons::UpButton;
      else if (DownButton_debounce_8bits == 0xFF) return LCDButtons::DownButton;
      else if (LeftButton_debounce_8bits == 0xFF) return LCDButtons::LeftButton;
      else if (SelectButton_debounce_8bits == 0xFF) return LCDButtons::SelectButton;
      else return LCDButtons::NoButtonPressed;
    }
  } else {
    return userInput;
  }
}
