#include "UserInterface.hpp"
#include "LCD1604Shield.hpp"

UserInterface::UserInterface()
{
  p_displayShield = new LCD1604Shield;
}

void UserInterface::displayShieldTest()
{
  static_cast<LCD1604Shield*>(p_displayShield)->RunTest1604Shield();
}
