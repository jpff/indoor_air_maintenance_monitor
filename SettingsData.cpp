#include "SettingsData.hpp"
#include <EEPROM.h>
#include <CRC32.h>

#ifndef Arduino_h
#include <Arduino.h>
#endif

SettingsData::SettingsData(const float m_firmware_version)
{
  if (!loadSettings()) // First load the settings off the EEPROM, make sure they were loaded correctly, otherwise reset them
  {
    SettingsData::resetToDefaults();
  }
  
  if (m_firmware_version > SettingsData::CurrentSettingsWrapper.firmware_version) // If this is a newer version of the firmware than the one used previously to save settings to EEPROM, then...
  {
    resetToDefaults();                                                            // Reset values
    SettingsData::CurrentSettingsWrapper.firmware_version = m_firmware_version;   // Update with the new version number
    saveSettings();                                                               // Then save the new default settings
  }
}

bool SettingsData::loadSettings()
{
  unsigned int l_EEPROM_Index = EEPROM_DATA_STARTS_AT;

  // Read settings
  SettingsData::readEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.firmware_version, &l_EEPROM_Index);
  
  SettingsData::readEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.is_indoors_DHTsensor_enabled, &l_EEPROM_Index);
  SettingsData::readEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.is_outdoors_DHTsensor_enabled, &l_EEPROM_Index);
  SettingsData::readEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.is_air_quality_sensor_enabled, &l_EEPROM_Index);

  SettingsData::readEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.LCD_lighting_mode, &l_EEPROM_Index);
  SettingsData::readEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.LCD_auto_lighting_threshold, &l_EEPROM_Index);

  SettingsData::readEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.indoors_DHTsensor_dataPin, &l_EEPROM_Index);
  SettingsData::readEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.outdoors_DHTsensor_dataPin, &l_EEPROM_Index);
  SettingsData::readEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.air_quality_sensor_dataPin, &l_EEPROM_Index);
  
  SettingsData::readEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.indoor_outdoor_temperature_difference_threshold, &l_EEPROM_Index);
  SettingsData::readEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.indoor_outdoor_humidity_difference_threshold, &l_EEPROM_Index);
  SettingsData::readEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.indoor_outdoor_heat_index_threshold, &l_EEPROM_Index);
  SettingsData::readEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.indoor_outdoor_relative_humidity_threshold, &l_EEPROM_Index);
  SettingsData::readEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.max_worst_value_for_indoor_airquality, &l_EEPROM_Index);
  
  SettingsData::readEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.standby_minutes_between_on_off_operations, &l_EEPROM_Index);
  
  SettingsData::readEEPROMData <unsigned long> (&SettingsData::CurrentSettingsWrapper.settings_checksum, &l_EEPROM_Index);

  // Calculate checksum and confirm if it matches
  if (calculateSettingsChecksum(&(SettingsData::CurrentSettingsWrapper)) == SettingsData::CurrentSettingsWrapper.settings_checksum)
  {
    return true;
  } else {
    return false;
  }
}

void SettingsData::saveSettings()
{
  unsigned int l_EEPROM_Index = EEPROM_DATA_STARTS_AT;

  // Calculate checksum
  SettingsData::CurrentSettingsWrapper.settings_checksum = calculateSettingsChecksum(&(SettingsData::CurrentSettingsWrapper));

  // Save current settings
  SettingsData::writeEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.firmware_version, &l_EEPROM_Index);
  
  SettingsData::writeEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.is_indoors_DHTsensor_enabled, &l_EEPROM_Index);
  SettingsData::writeEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.is_outdoors_DHTsensor_enabled, &l_EEPROM_Index);
  SettingsData::writeEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.is_air_quality_sensor_enabled, &l_EEPROM_Index);

  SettingsData::writeEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.LCD_lighting_mode, &l_EEPROM_Index);
  SettingsData::writeEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.LCD_auto_lighting_threshold, &l_EEPROM_Index);

  SettingsData::writeEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.indoors_DHTsensor_dataPin, &l_EEPROM_Index);
  SettingsData::writeEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.outdoors_DHTsensor_dataPin, &l_EEPROM_Index);
  SettingsData::writeEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.air_quality_sensor_dataPin, &l_EEPROM_Index);
  
  SettingsData::writeEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.indoor_outdoor_temperature_difference_threshold, &l_EEPROM_Index);
  SettingsData::writeEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.indoor_outdoor_humidity_difference_threshold, &l_EEPROM_Index);
  SettingsData::writeEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.indoor_outdoor_heat_index_threshold, &l_EEPROM_Index);
  SettingsData::writeEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.indoor_outdoor_relative_humidity_threshold, &l_EEPROM_Index);
  SettingsData::writeEEPROMData <float> (&SettingsData::CurrentSettingsWrapper.max_worst_value_for_indoor_airquality, &l_EEPROM_Index);

  SettingsData::writeEEPROMData <int> (&SettingsData::CurrentSettingsWrapper.standby_minutes_between_on_off_operations, &l_EEPROM_Index);
  
  SettingsData::writeEEPROMData <unsigned long> (&SettingsData::CurrentSettingsWrapper.settings_checksum, &l_EEPROM_Index);
}

void SettingsData::resetToDefaults()
{
  SettingsData::CurrentSettingsWrapper = SettingsData::DefaultSettingsWrapper;
}

unsigned long SettingsData::calculateSettingsChecksum(void *l_structPointer)
{




  
}

/*
unsigned long SettingsData::calculateSettingsChecksum(void *l_structPointer)
{  
  unsigned int l_sizeOfData = sizeof(SettingsData::SettingsWrapperTemplate) - sizeof(SettingsData::SettingsWrapperTemplate::settings_checksum);   // Exclude the checksum from the checksum itself
  uint8_t l_temp_StructByteArray[l_sizeOfData];
  
  memcpy(l_temp_StructByteArray, l_structPointer, l_sizeOfData);          // Copy the structure to the byte array
  return (CRC32::calculate(l_temp_StructByteArray, l_sizeOfData - 1));    // Calculate checksum
}
*/

template <typename T> void SettingsData::rebuildTypeFromByteArray (T* l_TypeDestination, byte* l_ArraySource)
{
  union DataUnion
  {
    byte DataUnionBytes[sizeof(T)];
    T DataTypeContainer;
  } DataByteUnion;

  // void * memcpy ( void * destination, const void * source, size_t num );
  memcpy(&DataByteUnion.DataUnionBytes, l_ArraySource, sizeof(T));
  memcpy(l_TypeDestination, &DataByteUnion.DataTypeContainer, sizeof(T));
}

template <typename T> void SettingsData::decomposeTypeToByteArray (T* l_TypeSource, byte* l_ArrayDestination)
{
  union DataUnion
  {
    byte DataUnionBytes[sizeof(T)];
    T DataTypeContainer;
  } DataByteUnion;

  memcpy(&DataByteUnion.DataTypeContainer, l_TypeSource, sizeof(T));
  memcpy(l_ArrayDestination, &DataByteUnion.DataUnionBytes, sizeof(T));
}

template <typename T> void SettingsData::readEEPROMData(T* l_dataDestination,  unsigned int* l_EEPROM_address)
{
  union DataUnion
  {
    byte DataUnionBytes[sizeof(T)];
    T DataTypeContainer;
  } DataByteUnion;

  for (unsigned int l_index = 0; l_index < sizeof(T); l_index++)
  {
    DataByteUnion.DataUnionBytes[l_index] = EEPROM.read(*l_EEPROM_address + l_index);
  }
  
  *l_dataDestination = DataByteUnion.DataTypeContainer;
  *l_EEPROM_address += sizeof(T);
}

template <typename T> void SettingsData::writeEEPROMData(T* l_dataToWrite, unsigned int* l_EEPROM_address)
{
  union DataUnion
  {
    byte DataUnionBytes[sizeof(T)];
    T DataTypeContainer;
  } DataByteUnion;

  DataByteUnion.DataTypeContainer = *l_dataToWrite;

  for (unsigned int l_index = 0; l_index < sizeof(T); l_index++)
  {
    EEPROM.update(*l_EEPROM_address + l_index, DataByteUnion.DataUnionBytes[l_index]);
  }
  
  *l_EEPROM_address += sizeof(T);
}
