#ifndef _LCD1604SHIELD_HPP_
#define _LCD1604SHIELD_HPP_

typedef class LCD1604Shield
{
  public:
    LCD1604Shield();
    bool setLCDText(const char*, int, int);
    int getUserInput();
    void clearScreen();
    void RunTest1604Shield();
    bool enableDebounce = true;
    bool enable16bitDebounce = true;
  private:
    void* p_LCD1604Shield;
    enum LCDResolution {x = 16, y = 2};
    enum LCDPins {rs = 8, enable = 9, d4 = 4, d5 = 5, d6 = 6, d7 = 7};
    enum LCDButtons {RightButton, UpButton, DownButton, LeftButton, SelectButton, NoButtonPressed};
    int readLCD_keypad();
};

#endif
