#ifndef _SETTINGSDATA_HPP_
#define _SETTINGSDATA_HPP_

#ifndef Arduino_h
#include <Arduino.h>
#endif

#define EEPROM_DATA_STARTS_AT 0   // The index, or address, at which the program should begin storing its settings at within the EEPROM

typedef class SettingsData
{
  public:
    typedef struct SettingsWrapperTemplate
    {
      float firmware_version;                                           // Current firmware version
      
      int is_indoors_DHTsensor_enabled;                                 // True or false
      int is_outdoors_DHTsensor_enabled;                                // True or false
      int is_air_quality_sensor_enabled;                                // True or false

      int LCD_lighting_mode;                                            // Mode 0 - always off, 1 - always on, 2 - automatic depending on environment lighting conditions with selectable threshold
      int LCD_auto_lighting_threshold;                                  // Threshold between on and off lighting modes

      int indoors_DHTsensor_dataPin;                                    // Arduino data pin that connects to the indoors DHT sensor
      int outdoors_DHTsensor_dataPin;                                   // Arduino data pin that connects to the outdoors DHT sensor
      int air_quality_sensor_dataPin;                                   // Arduino data pin for Grove Air Quality Sensor, needs to be an analog input pin

      float indoor_outdoor_temperature_difference_threshold;            // Max temperature difference between the outside and the inside, Celcius
      float indoor_outdoor_humidity_difference_threshold;               // Max humidity difference between the outside and the inside
      float indoor_outdoor_heat_index_threshold;                        // Perceived heat, celcius
      float indoor_outdoor_relative_humidity_threshold;                 // Relative humidity, percentage
      float max_worst_value_for_indoor_airquality;                      // In percentage

      int standby_minutes_between_on_off_operations;                    // The number of minutes to wait between turning on and off the fan/blower between operations, even if any of the temperature or humidity values exceed the threshold
      
      unsigned long settings_checksum;                                  // The integrity checksum for the settings structure
    } SETTINGSWRAPPERTEMPLATE;

    SETTINGSWRAPPERTEMPLATE CurrentSettingsWrapper;

    SettingsData(const float);
    bool loadSettings();
    void saveSettings();
    void resetToDefaults();
    
  private:
    // DATA WRAPPER WITH DEFAULT SETTINGS
    const SETTINGSWRAPPERTEMPLATE DefaultSettingsWrapper = {
      1.0,  // firmware_version
      1,    // is_indoors_DHTsensor_enabled
      1,    // is_outdoors_DHTsensor_enabled
      1,    // is_air_quality_sensor_enabled
      0,    // LCD lighting mode, 0 - always off, 1 - always on, 2 - automatic
      700,  // auto LCD lighting mode
      22,   // indoors_DHTsensor_dataPin
      24,   // outdoors_DHTsensor_dataPin
      A15,  // air_quality_sensor_dataPin
      2,    // indoor_outdoor_temperature_difference_threshold
      15,   // indoor_outdoor_humidity_difference_threshold
      2,    // indoor_outdoor_heat_index_threshold
      5,    // indoor_outdoor_relative_humidity_threshold
      70,   // max_worst_value_for_indoor_airquality
      30,   // standby_minutes_between_on_off_operations
      0     // settings_checksum
    };

    template <typename T> void rebuildTypeFromByteArray(T*, byte*);
    template <typename T> void decomposeTypeToByteArray (T*, byte*);
    template <typename T> void readEEPROMData(T*, unsigned int*);             // void* dataDestination, int* dataAddress. Warning: Will auto increment data index pointer.
    template <typename T> void writeEEPROMData(T*, unsigned int*);            // void* dataDestination, int* dataAddress. Warning: Will auto increment data index pointer.
    unsigned long calculateSettingsChecksum(void*);                           // Calculates settings integrity checksum
};

#endif
