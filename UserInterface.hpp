#ifndef _USERINTERFACE_HPP_
#define _USERINTERFACE_HPP_

typedef class UserInterface
{
  public:
    UserInterface();
    void displayShieldTest();
  private:
    void* p_displayShield;
};

#endif
