#include "MonitorSensors.hpp"
#include "DHT.h"

MonitorSensors::MonitorSensors()  {}

void MonitorSensors::enableIndoorsDHT(bool l_state)
{
  if(l_state)
  {
    if(p_IndoorsDHTSensor == NULL)
    {
      p_IndoorsDHTSensor = new DHT(DHT_INDOORS_DATAPIN, DHT11);
      static_cast<DHT*>(p_IndoorsDHTSensor)->begin();
    }
  } else {
    if(p_IndoorsDHTSensor != NULL)
    {
      delete (static_cast<DHT*>(p_IndoorsDHTSensor));
    }
  }
}

void MonitorSensors::enableOutdoorsDHT(bool l_state)
{
  if(l_state)
  {
    if(p_OutdoorsDHTSensor == NULL)
    {
      p_OutdoorsDHTSensor = new DHT(DHT_OUTDOORS_DATAPIN, DHT11);
      static_cast<DHT*>(p_OutdoorsDHTSensor)->begin();
    }
  } else {
    if(p_OutdoorsDHTSensor != NULL)
    {
      delete (static_cast<DHT*>(p_OutdoorsDHTSensor));
    }
  }
}

void MonitorSensors::enableIndoorsAirQuality(bool l_state)
{
  if(l_state)
  {
    if(p_IndoorsAirQualitySensor == NULL)
    {
      
    }
  } else {
    if(p_IndoorsAirQualitySensor != NULL)
    {
      delete p_IndoorsAirQualitySensor;
    }
  }
}

float MonitorSensors::DHT_getIndoorsTemperature()
{
  if(p_IndoorsDHTSensor != NULL)
  {
    return (static_cast<DHT*>(p_IndoorsDHTSensor)->readTemperature());
  } else {
    return 0;
  }
}

float MonitorSensors::DHT_getOutdoorsTemperature()
{
  if(p_OutdoorsDHTSensor != NULL)
  {
    return (static_cast<DHT*>(p_OutdoorsDHTSensor)->readTemperature());
  } else {
    return 0;
  }
}

float MonitorSensors::DHT_getIndoorsHumidity()
{
  if(p_IndoorsDHTSensor != NULL)
  {
      return (static_cast<DHT*>(p_IndoorsDHTSensor)->readHumidity());
  } else {
    return 0;
  }
}

float MonitorSensors::DHT_getOutdoorsHumidity()
{
  if(p_OutdoorsDHTSensor != NULL)
  {
    return (static_cast<DHT*>(p_OutdoorsDHTSensor)->readHumidity());
  } else {
    return 0;
  }
}

float MonitorSensors::DHT_getIndoorsHeatIndex(float l_temperature, float l_humidity)
{
  if(p_IndoorsDHTSensor != NULL)
  {
    return (static_cast<DHT*>(p_IndoorsDHTSensor)->computeHeatIndex(l_temperature, l_humidity, false));
  } else {
    return 0;
  }
}

float MonitorSensors::DHT_getOutdoorsHeatIndex(float l_temperature, float l_humidity)
{
  if(p_OutdoorsDHTSensor != NULL)
  {
    return (static_cast<DHT*>(p_OutdoorsDHTSensor)->computeHeatIndex(l_temperature, l_humidity, false));
  } else {
    return 0;
  }
}

float MonitorSensors::AirQuality_getIndoorsAirQuality()
{
  if(p_IndoorsAirQualitySensor != NULL)
  {
    
  }
}
